<?php 

/* Exercice 0 : */

$Nombre = 5;
echo "$Nombre<br>";
echo "<br>";

/* Exercice 1 : */

$word = "c'est ";
$word2 = "vie.";
$word3 = "Simplon ";
$word4 = "la ";
$phrase = $word3 . $word . $word4 . $word2;
echo "$phrase<br>";
echo "<br>";

/* Exercice 2 */

$variable1 = 12;
$variable2 = 4;
$variable3 = $variable1 / $variable2;
echo "$variable3<br>";
echo "<br>";

/* Exercice 3 */

$var ="clement";
$var1 = "extrapolation";
$var2 = $var . $var1;
echo strlen("$var2");
echo "<br>";

/* Exercice 4 */

$maîtrise_du_code = 1000;

if ( $maîtrise_du_code >= 1000) {
echo "je maîtrise tellement le code maintenant ! <br>";
}
echo "<br>";

/* Exercice 5 */

$chiffre_fetiche_sandrine = 7;
$chiffre_fetiche_xavier = 130;
$chiffre_fetiche_andre = 8;

$chiffre_fetiche_xavier = $chiffre_fetiche_sandrine;
$chiffre_fetiche_sandrine = $chiffre_fetiche_xavier;
$chiffre_fetiche_xavier = $chiffre_fetiche_sandrine . $chiffre_fetiche_andre;
$erreur = "Le nombre est supérieur ou égale à 50";

if ($chiffre_fetiche_xavier < 50) {
    echo "$chiffre_fetiche_xavier<br>";
} else {
    echo "$erreur<br>";
}
echo "<br>";

/* Exercice 6 */

$compte = -100;

if ($compte > 0) {
    echo "Bravo, vous êtes un bon gestionnaire!<br>";
} else {
    echo "Vous faites vraiment n'importe quoi avec votre argent..<br>";
}
echo "<br>";

/* Exercice 7 */

$vent = 15 ;
$houle = 26 ;
$cadence_vague = 13;

if($vent > 30 && $houle < 20 && $cadence_vague >= 10){
    echo "On peut aller surfer ! ";
} elseif ($vent < 30 && $houle < 30 && $cadence_vague <= 8) {
    echo "On peut aller surfer!";
} else {
    echo "Les conditions ne sont pas bonnes pour le surf !";
}
echo "<br>";

/* Exercice 8 */

$nombre_1 = 88;
$nombre_2 = 7;
$nombre_3 = 23;
$nombre_4 = 5;
$nombre_5 = 45;
$nombre_6 = 12;
$nombres = [$nombre_1 => 88, $nombre_2 => 7,$nombre_3 => 23,
$nombre_4 => 5, $nombre_5 => 45, $nombre_6 => 12];
$additionP = 0;
$additionI = 0;
$soustraction = 0;   // Je déclare mes variables additions paires et impaires et la variable soustraction. 


foreach ($nombres as $nombre => $value) {
    if($nombre % 2 == 0) {
       $additionP += $value;
    }else $additionI += $value;
}
echo $soustraction = $additionP - $additionI;